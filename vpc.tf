resource "aws_vpc" "terraform-jenkins-vpc" {
    cidr_block = var.vpc_cidr
    instance_tenancy = var.tenancy
    tags = {
        Name = "${terraform.workspace}-terraform-jenkins-vpc"
    }
}