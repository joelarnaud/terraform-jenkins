resource "aws_instance" "new-ec2"{
    ami = "ami-0c2f25c1f66a1ff4d"
    instance_type = "t2.micro"
    vpc_security_group_ids = ["${aws_security_group.new-ec2_sg.id}"]
    key_name = "keypair"
    tags = {
        Name = "new-ec2"
    } 

}