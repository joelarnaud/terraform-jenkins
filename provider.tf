provider "aws"{
    region = var.region
}

terraform {
    backend "s3" {
        bucket = "terraform-state-1984"
        key = "terraform.tfstate"
        region = "ca-central-1"
    }
}